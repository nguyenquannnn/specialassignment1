/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.specialasignment1;

import java.io.Serializable;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *  Backing bean for the index.xhtml view
 * 
 * @author 1632647
 */
@Named("exampleModel")
@SessionScoped
public class ExampleModel implements Serializable {
    //Binding field to the text input
    private String input;
    
    //Regex pattern to check for no capital letters
    private Pattern noCapitalLetters;
    
    //Initialize stuff
    @PostConstruct
    public void init() {
        noCapitalLetters = Pattern.compile("^[a-z]*$");
        input = "";
    }
    
    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
    
    //Throw Runtime error for onError to catches if there is a capital letter or null field
    //Throw Runtime error for confirming submission
    public void update() {
        if(input == null || !input.matches(noCapitalLetters.pattern())) {
            input = "";
            throw new RuntimeException("No capital letters allowed I said >:(");
        } else {
            throw new RuntimeException("Hey you didn't make mistake >:(");
        }
    }
}
